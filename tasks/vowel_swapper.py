def vowel_swapper(string):
    # ==============
    # Your code here
    
    array = list(string)

    convert = str()

    for x in range(0, len(array)):
        if array[x] == 'a' or array[x] == 'A': array[x] = '4'
        if array[x] == 'e' or array[x] == 'E': array[x] = '3'
        if array[x] == 'i' or array[x] == 'I': array[x] = '!'
        if array[x] == 'u' or array[x] == 'U': array[x] = 'l_l'
        if array[x] == 'o': array[x] = 'ooo'
        if array[x] == 'O': array[x] = '000'
        convert = convert + array[x]
    
    return(convert)
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
