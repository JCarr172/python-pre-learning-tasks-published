def vowel_swapper(string):
    # ==============
    # Your code here

    array = list(string)

    convert = str()
    a=0
    e=0
    i=0
    o=0
    u=0
    for x in range(0, len(array)):
        if array[x] == 'a' or array[x]== 'A':
            if a == 1:
                array[x] = '4'
                a = a + 1
            else:
                a = a + 1
        if array[x] == 'e' or array[x] == 'E':
            if e == 1:
                array[x] = '3'
                e = e + 1
            else:
                e = e + 1
        if array[x] == 'i' or array[x] == 'I':
            if i == 1:
                array[x] = '!'
                i = i + 1
            else:
                i = i + 1
        if array[x] == 'u' or array[x] == 'U':
            if u == 1:
                array[x] = 'l_l'
                u = u + 1
            else:
                u = u + 1
        if array[x] == 'o':
            if o == 1:
                array[x] = 'ooo'
                o = o + 1
            else:
                o = o + 1
        if array[x] == 'O':
            if o == 1:
                array[x] = '000'
                o = o + 1
            else:
                o = o + 1
        convert = convert + array[x]
    
    return(convert)

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
